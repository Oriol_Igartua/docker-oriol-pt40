# Activitat V Serveis de Xarxa (Docker)
# Oriol Igartua

1. Contenidor Apache2:
**1.1** En aquest pas instal.larem a dins d'un contenidor el paquet Apache2, ho farem com sempre, prèviament haurem actualitzat el sistema:

![](Pictures/Screenshot_20201209_203900.png)

A continuació anirem a crear la nostre pàgina web que volguem que aparegui en el nostre servdor apache.
Ho farem amb el "echo".

![](Pictures/eqeqe.png)

**1.2** Aquí engegarem el servidor web, o farem iniciant el servei amb la comanda:

**$apache2ctl restart**

![](Pictures/Screenshot_20201212_180318.png)

**1.3** En aquest apartat mirarem quin ip té el contenidor mitjantçant el ús de la següent comanda:

**hostname -I**

![](Pictures/Screenshot_20201212_180445.png)

**1.4** Apuntarem a la ip del nostre contenidor i posarem el port, en el nostre cas encara no ho hem configurat peró ho farem a continuació, com podem veure.

![](Pictures/Screenshot_20201212_180735.png)

![](Pictures/Screenshot_20201212_180931.png)

Per redireccionar la pàgina al port 8080 haurem de configurar el arxiu default-ssl que es troba a /etc/apache2/sites-available/

Ho hem fet amb un echo:

![](Pictures/Screenshot_20201212_182330.png)

**1.6** Aquí farem el commit del contenidor amb el nostre nom:

**docker commit (nom_container) nom_imatge**

![](Pictures/Screenshot_20201212_183806.png)

**1.7** Cambiarem el tag de la imatge  i posarem el nostre nom de repositori a davant d'aquesta:

(Clar aclarar que cambiant l'espai del disc dur de la màquina virtual no puc accedir a l'interficie gràfica)

Ara pujarem la imatge a dockerfile utilitzant el seu tag:

![](Pictures/Screenshot_20201212_194243.png)

**1.8** Finalment en aquest pas comprovaber que la imatge s'ha penjat correctament a DockerHub:

![](Pictures/Screenshot_20201212_194539.png)
